from PySide2.QtWidgets import QWidget
from PySide2.QtCore import Signal



class ClickableWidget(QWidget):
    
    clicked = Signal(object)
    
    def  __init__(self, parent=None):
        super(ClickableWidget, self).__init__(parent=parent)

    def mouseReleaseEvent(self, a0):
        self.clicked.emit(self)
        return super().mouseReleaseEvent(a0)
        