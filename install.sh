#!/bin/bash

set -e
sudo -v

EII_GUI_PATH=$PWD

display_progress () {
     echo " "
     echo "------------------------------------------------------"
     echo $1
     echo "------------------------------------------------------"
     echo " "
}

install_eii_gui_dependencies () {
     display_progress "Installing EII Deployment Tool dependencies..."

     # Install apt pkg
     sudo apt-get update
     sudo apt-get install -y vim python3-pip python3-pyqt5 libxcb-xinerama0 sshpass openssl git openssh-server
     sudo apt-get remove -y python3-yaml

     # Set pip mirror
     PIP_CONF=$HOME/.pip/pip.conf
     mkdir -p $HOME/.pip
     if [ ! -f "$PIP_CONF" ]; then
          touch "$PIP_CONF"
          echo "[global]" >> "$PIP_CONF"
          echo "index-url = https://pypi.tuna.tsinghua.edu.cn/simple/" >> "$PIP_CONF"
          echo "[install]" >> "$PIP_CONF"
          echo "trusted-host=pypi.tuna.tsinghua.edu.cn" >> "$PIP_CONF"
     fi

     # Upgrade pip3
     sudo -E pip3 install --upgrade pip

     # Install python virtualenv 
     sudo -E pip3 install virtualenv virtualenvwrapper
     mkdir -p $HOME/.virtualenvs

     # Install python dependencies in virtual environment
     export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
     source /usr/local/bin/virtualenvwrapper.sh
     mkvirtualenv -p /usr/bin/python3 eiigui-env && workon eiigui-env && pip3 install -r setup/requirements.txt

     # Set EII_GUI_HOME and add to PATH
     sed -i 's!EII_GUI_HOME=.*!EII_GUI_HOME='$EII_GUI_PATH'!g' $EII_GUI_PATH/eiigui
     sudo cp $EII_GUI_PATH/eiigui /usr/local/bin
}

system_fix () {
     # Solve network setting missing
     if [ -f "/etc/NetworkManager/conf.d/10-globally-managed-devices.conf" ]; then
          sudo rm -rf /etc/NetworkManager/conf.d/10-globally-managed-devices.conf
     fi
     sudo touch /etc/NetworkManager/conf.d/10-globally-managed-devices.conf
}

download_eii_lib () {
     display_progress "Downloading EII Libs..."

     cd $EII_GUI_PATH/setup
     # EIIMessageBus
     if [ -d "./eii-msgbus/EIIMessageBus" ];then
          sudo rm -rf ./eii-msgbus/EIIMessageBus
     fi
     git clone https://gitee.com/open-edge-insights/eii-messagebus.git --branch v2.6.1
     rm -rf eii-messagebus/.git
     mv eii-messagebus EIIMessageBus
     cp -r EIIMessageBus eii-msgbus
     rm -rf EIIMessageBus
     # EIIUtils
     if [ -d "./eii-msgbus/EIIUtils" ];then
          sudo rm -rf ./eii-msgbus/EIIUtils
     fi
     git clone https://gitee.com/open-edge-insights/eii-c-utils.git --branch v2.6.1
     rm -rf eii-c-utils/.git
     mv eii-c-utils EIIUtils
     cp -r EIIUtils eii-msgbus
     rm -rf EIIUtils
     # src-gst-gencamsrc
     if [ -d "./src-gst-gencamsrc" ];then
          sudo rm -rf ./src-gst-gencamsrc
     fi
     git clone https://gitee.com/open-edge-insights/video-ingestion.git --branch v2.6.1
     cp -r video-ingestion/src-gst-gencamsrc .
     rm -rf video-ingestion
     echo "Finished download eii libs"

     grep -nr "https://github.com/DaveGamble.*.gz" | awk -F ":" '{print$1}' | xargs sed -i 's!https://github.com/DaveGamble/cJSON/archive/v${cjson_version}.tar.gz!https://gitee.com/xiaowei315/eii_3rd_pkg/attach_files/786633/download/cJSON-${cjson_version}.tar.gz!g'
     grep -nr "https://github.com/zeromq.*.gz" | awk -F ":" '{print$1}' | xargs sed -i 's!https://github.com/zeromq/libzmq/releases/download/v4.3.3/zeromq-${zeromq_version}.tar.gz!https://gitee.com/xiaowei315/eii_3rd_pkg/attach_files/892628/download/zeromq-${zeromq_version}.tar.gz!g'
     wje_path=$EII_GUI_PATH/setup/eii-msgbus/EIIUtils/install.sh
     safestr_path=$EII_GUI_PATH/setup/eii-msgbus/EIIUtils/IntelSafeString/cmake/IntelSafeString.txt.in
     msg_googletest_path=$EII_GUI_PATH/setup/eii-msgbus/EIIMessageBus/cmake/GoogleTestCMakeLists.txt.in
     util_googletest_path=$EII_GUI_PATH/setup/eii-msgbus/EIIUtils/cmake/GoogleTestCMakeLists.txt.in
     sed -i 's!https://github.com/netmail-open/wjelement!https://gitee.com/xiaowei315/wjelement!g' $wje_path
     sed -i 's!https://github.com/intel/safestringlib!https://gitee.com/mirrors_intel/safestringlib!g' $safestr_path
     sed -i 's!https://github.com/google/googletest!https://gitee.com/xiaowei315/googletest!g' $msg_googletest_path
     sed -i 's!https://github.com/google/googletest!https://gitee.com/xiaowei315/googletest!g' $util_googletest_path
     grep -nr "install.* -r .*requirements.txt" | awk -F ":" '{print$1}' | grep -v Binary | xargs sed -i 's!install.* -r .*requirements.txt!& -i https://mirrors.aliyun.com/pypi/simple!g'
}

install_eii_msgbus () {
     display_progress "Installing EII Msgbus and Utils..."

     cd $EII_GUI_PATH/setup/eii-msgbus
     chmod +x setup.sh
     ./setup.sh
}

install_matrix_vision () {
     display_progress "Installing Matrix Vision and GStreamer Plugin..."

     # Install dependency
     sudo apt-get update
     sudo apt install -y libboost-python-dev unzip build-essential autoconf make pciutils cpio libtool lsb-release ca-certificates pkg-config bison flex zlib1g-dev automake net-tools iproute2 libgtk2.0-dev
     sudo apt install -y libgstreamer1.0-0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-doc gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev

     # Install Matrix Vision Camera SDK
     if [ -z "$(which wxPropView)" ]; then
         MATRIX_VISION_SDK_VER=2.38.0
         mkdir -p $EII_GUI_PATH/matrix_vision_downloads
         cd $EII_GUI_PATH/matrix_vision_downloads
         wget https://gitee.com/xiaowei315/eii_3rd_pkg/attach_files/847516/download/mvGenTL_Acquire-x86_64_ABI2-${MATRIX_VISION_SDK_VER}.tgz
         wget https://gitee.com/xiaowei315/eii_3rd_pkg/attach_files/847515/download/install_mvGenTL_Acquire.sh
         chmod +x install_mvGenTL_Acquire.sh
         yes "" | ./install_mvGenTL_Acquire.sh
         cd $EII_GUI_PATH
         rm -rf matrix_vision_downloads
     fi

     # Install genicamsrc plugin
     set +e
     cd $EII_GUI_PATH/setup/src-gst-gencamsrc
     mkdir -p ./m4
     autoreconf -i
     autoreconf -i
     sudo ./setup.sh
     set -e

     # Remove unuse file
     cd $EII_GUI_PATH
     if [ -f "ltmain.sh" ]; then
          rm -rf ltmain.sh
     fi
}

download_udf_samples () {
     display_progress "Downloading UDF Samples..."

     cd $EII_GUI_PATH/eii_helper
     if [ -d "udf-samples" ]; then
          rm -rf udf-samples
     fi
     git clone https://gitee.com/open-edge-insights/udf-samples.git
}

install_realsense () {
     display_progress "Installing RealSense SDK 2.0..."

     if [ -z "$(which realsense-viewer)" ]; then
          if [ -z "$http_proxy" ]; then
               sudo apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
          else
               sudo apt-key adv --keyserver keys.gnupg.net --keyserver-options http-proxy="$http_proxy" --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --keyserver-options http-proxy="$http_proxy" --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
          fi
          sudo add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo $(lsb_release -cs) main" -u
          sudo apt-get install -y librealsense2-dkms librealsense2-utils librealsense2-dev librealsense2-dbg
          echo -e "\033[32m realsense-viewer installed successfully \033[0m"
     fi
}

run_private_registry () {
     display_progress "Setup Docker Registry..."

     if [ "$(which docker)" ]; then
          sudo docker ps -q --filter "name=eiigui-registry" | grep -q . && : || (sudo docker run -d -p 5050:5000 --restart=always --name eiigui-registry registry:2)
     else
          echo "Docker is not installed!"
     fi
}


startTime=`date +%Y%m%d-%H:%M:%S`
startTime_s=`date +%s`

install_eii_gui_dependencies
system_fix
download_eii_lib
install_eii_msgbus
install_matrix_vision
download_udf_samples
# install_realsense
# run_private_registry

display_progress "Success to install EII Deployment Tool!"

endTime=`date +%Y%m%d-%H:%M:%S`
endTime_s=`date +%s`
sumTime=$[ $endTime_s - $startTime_s ]
echo "$startTime ---> $endTime" "Total: $sumTime seconds"
echo " "