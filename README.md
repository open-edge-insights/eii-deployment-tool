# EII Deployment Tool

## Prerequisites and Requirements

1. Install Ubuntu20.04.
    * USB boot tool "Ventoy": https://www.ventoy.net/cn/index.html
    * Ubuntu20.04 download link: https://ubuntu.com/download/desktop

2. Install EII. 

    - ESH Link: [web link](https://www.intel.com/content/www/us/en/developer/topic-technology/edge-5g/edge-solutions/industrial-recipes.html)

    - Github Link: [web link](https://github.com/open-edge-insights/eii-manifests)

    - Gitee Link (PRC network recommand): [web link](https://gitee.com/open-edge-insights/eii-install-guide)

## Get Started for EII Deployment Tool

Detail User Guide: [EII Deploy Tool User Guide](https://gitee.com/open-edge-insights/eii-install-guide/blob/master/user_guide/eii_deploy_tool/eiigui_user_guide.md)

1. Install EII Deployment Tool.

    ```
    $ ./install.sh
    ```

2. Launch EII Deployment Tool.

    ```
    $ eiigui
    ```

## How to uninstall EII Deployment Tool

* Run below command to uninstall.

    ```
    $ ./uninstall.sh
    ```

## Setup Target Device

* To setup environment for target device, please run below command:

    ```
    $ sudo ./target_device_setup.sh
    ```
