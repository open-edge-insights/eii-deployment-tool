from PySide2.QtWidgets import *
from PySide2.QtGui import QFont
from PySide2.QtCore import QSize


class CircleButton(QPushButton):

    def __init__(self, char="+", radius=12, font_size=16, parent=None):
        super(CircleButton, self).__init__(parent=parent)
        self.setText(char)
        diameter = radius * 2
        self.setMinimumSize(QSize(diameter, diameter))
        self.setMaximumSize(QSize(diameter, diameter))
        self.setStyleSheet(
            "background-color: rgb(0, 125, 196);"
            "color: rgb(255, 255, 255);"
            "padding-bottom: 4px; border: 1px solid black; border-radius: {};".format(radius))
        font = QFont()
        font.setPointSize(font_size)
        self.setFont(font)
