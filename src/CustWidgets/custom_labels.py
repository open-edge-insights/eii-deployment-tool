import math

from PySide2.QtWidgets import QLabel
from PySide2.QtCore import Qt, QSize, QPoint
from PySide2.QtGui import QFont, QPainter, QPen, QColor, QPolygon, QBrush

_ARROW_COLOR = QColor(70, 180, 0)


class HArrowLabel(QLabel):

    def __init__(self, parent=None, arrow_pos="right", solid=True):
        super(HArrowLabel, self).__init__(parent)
        self._arrow_pos = arrow_pos
        self._solid = solid
        self._arrow_size = 11

    def paintEvent(self, e):
        painter = QPainter()
        painter.begin(self)

        self.w = self.geometry().width()
        self.h = self.geometry().height()

        # Draw line first
        if self._solid:
            pen = QPen(_ARROW_COLOR, 2, Qt.SolidLine)
        else:
            pen = QPen(_ARROW_COLOR, 2, Qt.DotLine)
        painter.setPen(pen)
        painter.drawLine(0, self.h / 2, self.w, self.h / 2)

        # Draw arrow
        pen = QPen(_ARROW_COLOR, 2, Qt.SolidLine)
        painter.setPen(pen)
        painter.setBrush(QBrush(_ARROW_COLOR, Qt.SolidPattern))
        if self._arrow_pos == "right":
            arrow = QPolygon([
                QPoint(self.w, self.h / 2),
                QPoint(self.w - self._arrow_size, self.h / 2 - self._arrow_size * math.tan(math.pi / 6)),
                QPoint(self.w - self._arrow_size, self.h / 2 + self._arrow_size * math.tan(math.pi / 6))
            ])
        else:
            arrow = QPolygon([
                QPoint(0, self.h / 2),
                QPoint(0 + self._arrow_size, self.h / 2 - self._arrow_size * math.tan(math.pi / 6)),
                QPoint(0 + self._arrow_size, self.h / 2 + self._arrow_size * math.tan(math.pi / 6))
            ])
        painter.drawPolygon(arrow)

        painter.end()


class V1ArrowLabel(QLabel):

    def __init__(self, parent=None, arrow_pos="top", solid=True, size=11):
        super(V1ArrowLabel, self).__init__(parent)
        self._arrow_pos = arrow_pos
        self._solid = solid
        self._arrow_size = size

    def paintEvent(self, e):
        painter = QPainter()
        painter.begin(self)

        self.w = self.geometry().width()
        self.h = self.geometry().height()

        # Draw line first
        if self._solid:
            pen = QPen(_ARROW_COLOR, 2, Qt.SolidLine)
        else:
            pen = QPen(_ARROW_COLOR, 2, Qt.DotLine)
        painter.setPen(pen)
        painter.drawLine(self.w / 2, 0, self.w / 2, self.h)

        # Draw arrow
        pen = QPen(_ARROW_COLOR, 2, Qt.SolidLine)
        painter.setPen(pen)
        painter.setBrush(QBrush(_ARROW_COLOR, Qt.SolidPattern))
        if self._arrow_pos == "top":
            arrow = QPolygon([
                QPoint(self.w / 2, 0),
                QPoint(self.w / 2 - self._arrow_size * math.tan(math.pi / 6), 0 + self._arrow_size),
                QPoint(self.w / 2 + self._arrow_size * math.tan(math.pi / 6), 0 + self._arrow_size)
            ])
        else:
            arrow = QPolygon([
                QPoint(self.w / 2, self.h),
                QPoint(self.w / 2 - self._arrow_size * math.tan(math.pi / 6), self.h - self._arrow_size),
                QPoint(self.w / 2 + self._arrow_size * math.tan(math.pi / 6), self.h - self._arrow_size)
            ])
        painter.drawPolygon(arrow)

        painter.end()


class LineLabel(QLabel):
    """
    0----------1----------2
    |                     |
    |                     |
    7                     3
    |                     |
    |                     |
    6----------5----------4
    """

    def __init__(self, parent=None, pt1="1", pt2="5"):
        super(LineLabel, self).__init__(parent)
        self.pt1 = pt1
        self.pt2 = pt2

    def paintEvent(self, e):
        painter = QPainter()
        painter.begin(self)

        self.w = self.geometry().width()
        self.h = self.geometry().height()
        self.pos_map = {
            "0": QPoint(0, 0),
            "1": QPoint(self.w / 2, 0),
            "2": QPoint(self.w, 0),
            "3": QPoint(self.w, self.h / 2),
            "4": QPoint(self.w, self.h),
            "5": QPoint(self.w / 2, self.h),
            "6": QPoint(0, self.h),
            "7": QPoint(0, self.h / 2),
        }

        pen = QPen(_ARROW_COLOR, 2, Qt.SolidLine)
        painter.setPen(pen)
        painter.drawLine(self.pos_map[self.pt1], self.pos_map[self.pt2])

        painter.end()


class KeyLabel(QLabel):

    def __init__(self, parent=None, font_size=11, win_width=180, win_height=-1):
        super(KeyLabel, self).__init__(parent=parent)
        self._font_size = int(font_size) if font_size > 11 else 11
        font = QFont()
        font.setPointSize(self._font_size)
        self.setFont(font)

        self._win_width = int(win_width) if win_width > 0 else 16777215
        self._win_height = int(win_height) if win_height > 0 else 16777215
        self.setMinimumSize(QSize(self._win_width, self._win_height))
        self.setMaximumSize(QSize(self._win_width, self._win_height))
        