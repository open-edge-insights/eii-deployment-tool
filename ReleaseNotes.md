# EII Deployment Tool

EII Deployment Tool will guide you through the process of configuring, testing, deploying your application for your project.

## Release Notes (v2.6)

* Implemented new user interface, which contains 4 stages of operation windows.
* Preparation Stage (Welcome Window)
    - Create new data stream
    - Open existing data stream
    - Navigate to deploy data streams
* Configuration Stage (Config Window)
    - Switch Mode (VI&VA mode, VI mode)
    - Module configuration (VI, VA, Output)
        * Module config setting
        * Camera tuning
        * Msg Bus setting
* Testing Stage (Test Window)
    - Config info review and update
    - Build images, and start/stop data stream stack
    - Inference result showing up
* Deploying Stage (Deploy Window)
    - Remote Deploying (sigle stream/multiple streams)
    - Local Deploying (sigle stream/multiple streams)
    - Offline deploying, USB package generation