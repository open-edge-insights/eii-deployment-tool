#!/bin/bash

# Setup EII Stack
# Usage: sudo ./target_device_setup.sh [--proxy="<proxy address with port number>"]

# Current working dir
working_dir=$(pwd)

# Variable for displaying log
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
MAGENTA=$(tput setaf 5)
NC=$(tput sgr0)
BOLD=$(tput bold)
INFO=$(tput setaf 3)

 # Variable to store user proxy provided by command line
USER_PROXY=""
FILE_NAME=$0

Usage()
{
    echo
	echo "${BOLD}${INFO}==================================================================================${NC}"
    echo
	echo "${BOLD}${GREEN}Usage :: sudo ./target_device_setup.sh [OPTION...] ${NC}"
    echo
	echo "${INFO}List of available options..."
    echo
	echo "${INFO}--proxy	proxies, required when the gateway/edge node running EII (or any of EII profile) is connected behind proxy"
    echo
	echo "${INFO}--help		display this help and exit"${NC}
    echo
	echo "${INFO}-h		    display this help and exit"${NC}
    echo
	echo "${BOLD}${GREEN}Note : If --proxy option is not provided then script will run without proxy${NC}"
    echo
	echo "Different use cases..."
	echo "${BOLD}${MAGENTA}

		- RUNS WITHOUT PROXY
		sudo $FILE_NAME

		- RUNS WITH PROXY
		sudo $FILE_NAME --proxy=\"child-prc.intel.com:913\"
	"
	echo "${INFO}===================================================================================${NC}"
	exit 1
}

parse_commandLine_args()
{
	if [ $# == "0" ];then
		return;
	fi
	
	# echo "${INFO}Reading the command line args...${NC}"
	for ARGUMENT in "$@"
	do
	    KEY=$(echo $ARGUMENT | cut -f1 -d=)
	    VALUE=$(echo $ARGUMENT | cut -f2 -d=)   

	    case "$KEY" in
		    --proxy) USER_PROXY=${VALUE} ;;   
		    --help) Usage ;; 
            -h) Usage ;; 
		     *) echo "${RED}Invalid arguments passed..${NC}"; Usage; ;;  
	    esac    
	done
}

check_root_user()
{
   echo "${INFO}Checking for root user...${NC}"
   if [[ $EUID -ne 0 ]]; then
    	echo "${RED}This script must be run as root.${NC}"
	echo "${GREEN}E.g. sudo ./<script_name>${NC}"
    	exit 1
   else
   	echo "${GREEN}Script is running with root user.${NC}"
	dpkg --configure -a
   fi
   return 0

}

install_pkg()
{
	echo "Installing the dependent pkgs"
	apt-get update
	apt-get install -y git-all openssh-server
	pyyaml=$(dpkg --get-selections python3-yaml 2>&1 | grep -i 'install$' | awk '{ print $2 }')
	if [[ $pyyaml == "install" ]]; then
	    apt-get remove -y python3-yaml
		echo "remove pyyaml"
	fi
	echo "dependent pkg installed completed"
}

change_source()
{
	echo "Changing the source file..."
	env_file="$working_dir/eii-core/.env"
	sed -i 's/DEV_MODE=.*/DEV_MODE=true/g' $env_file
	pre_requisitesfile="$working_dir/eii-core/build/pre_requisites.sh"
	sed -i 's!google.com!baidu.com!g' $pre_requisitesfile
	sed -i 's!https://github.com/docker/compose/releases/download/${REQ_DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)!https://gitee.com/xiaowei315/eii_3rd_pkg/attach_files/802728/download/docker-compose-$(uname -s)-$(uname -m)-${REQ_DOCKER_COMPOSE_VERSION}!g' $pre_requisitesfile
}

get_requirests()
{
	echo "git clone the pre_requisites scripts"
	git clone https://gitee.com/open-edge-insights/eii-core.git $working_dir/eii-core
	change_source
	cd eii-core/build
	echo "perform pre_requisites scripts"
	if [ -z $USER_PROXY -ne "" ];then
		./pre_requisites.sh
	else
		./pre_requisites.sh --proxy=$USER_PROXY
	fi
}


check_root_user
if [ $# -ne "0" ];then
	parse_commandLine_args "$@"
fi
install_pkg
get_requirests

