import os
import secrets
import shutil
from enum import Enum

class EII_MODULE(Enum):
    VI = "VideoIngestion"
    VA = "VideoAnalytics"
    EtcdUI = "EtcdUI"
    WebVis = "WebVisualizer"
    ImageStore = "ImageStore"
    OPCUA = "OpcuaExport"
    TCP = "TcpExport"

class EII_FILE_NAME(Enum):
    CONFIG_JSON = "config.json"
    DOCKER_COMPOSE = "docker-compose.yml"
    DOCKER_COMPOSE_BUILD = "docker-compose-build.yml"
    PROJECT_COMP = "project_components.yml"

def get_random_tcp_output_port():
    port = secrets.SystemRandom().randint(50000, 54999)
    return str(port)

def get_random_server_port():
    port = secrets.SystemRandom().randint(55000, 59999)
    return str(port)

def get_random_msgbus_port():
    port = secrets.SystemRandom().randint(60000, 65534)
    return str(port)

def get_eiigui_root_path():
    cur_path = os.path.abspath(__file__)
    eiigui_root = os.path.dirname(os.path.dirname(os.path.dirname(cur_path)))
    return eiigui_root

def copy_files_under_dir(src_path, dst_path):
    if not os.path.exists(src_path):
        return
    if not os.path.exists(dst_path):
        os.makedirs(dst_path, exist_ok=True)

    file_list = os.listdir(src_path)
    for item in file_list:
        item_path = os.path.join(src_path, item)
        target_path = os.path.join(dst_path, item)
        if os.path.isfile(item_path):
            shutil.copy(item_path, target_path)
        elif os.path.isdir(item_path):
            if os.path.exists(target_path):
                shutil.rmtree(target_path)
            shutil.copytree(item_path, target_path)

if __name__ == "__main__":
    pass