#!/bin/bash

# Current working dir
working_dir=$(pwd)

# Variable for displaying log
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
MAGENTA=$(tput setaf 5)
NC=$(tput sgr0)
BOLD=$(tput bold)
INFO=$(tput setaf 3)

set -e

parse_commandLine_args()
{
	if [ $# == "0" ];then
		return;
	fi

	# echo "${INFO}Reading the command line args...${NC}"
	for ARGUMENT in "$@"
	do
	    KEY=$(echo $ARGUMENT | cut -f1 -d=)
	    VALUE=$(echo $ARGUMENT | cut -f2 -d=)

	    case "$KEY" in
		    --help) Usage ;;
		        -h) Usage ;;
		    --load) load_images_up ;;
		    --start) start_case ;;
		    --stop) stop_case ;;
		    --remove) remove_images ;;
		     *) echo "${RED}Invalid arguments passed..${NC}"; Usage; ;;
	    esac
	done
}

load_images_up()
{
	ls *.tar | xargs -n1 -I {} docker load -i {}
	start_case
}

start_case()
{
	cd $working_dir/build/provision
	sudo -E ./provision.sh ../docker-compose.yml
	cd ..
	docker-compose up -d
}

stop_case()
{
	docker ps -a | grep -i eiigui | awk -F " " '{print $1}' | xargs -n1 -I {} docker rm -f {}
	docker ps -a | grep -i openedgeinsights | awk -F " " '{print $1}' | xargs -n1 -I {} docker rm -f {}
	# cd $working_dir/build
	# docker-compose down
}

remove_images()
{
	docker images | grep -i eiigui | awk -F " " '{print $3}' | xargs -n1 -I {} docker rmi -f {}
}


if [ $# -ne "0" ];then
	parse_commandLine_args "$@"
else
	echo "${RED}At least one parameter is required${NC}"
	exit 1
fi

