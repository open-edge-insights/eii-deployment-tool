import os
import sys
import json
import yaml
import getpass
import subprocess as sp
import cv2
import numpy as np
import eii.msgbus as mb

from PySide2.QtWidgets import *
from PySide2.QtCore import Qt, QSize, QThread, Signal
from PySide2.QtGui import QFont, QImage, QPixmap

from CustStyleSheets import *
from CustWidgets import KeyLabel, PasswordDialog
from CustWidgets import V1ArrowLabel
from utils.eii_util import EII_FILE_NAME, get_eiigui_root_path

datastream_labels = ""

class TestWgt(QWidget):

    def __init__(self, parent=None, root_win=None, 
                 log_console=None, log_proc_flag=None, 
                 eii_home="", project_name="", pwd=""):
        super(TestWgt, self).__init__(parent=parent)
        self.root_win = root_win
        self.log_console = log_console
        self.log_proc_flag = log_proc_flag
        self.eii_home = eii_home
        self.eii_build_path = os.path.join(self.eii_home, "build")
        self.project_name = project_name
        self.project_path = os.path.join(self.eii_home, 
            "eii_helper/workspace", self.project_name)

        eii_dep_tool_root = get_eiigui_root_path()
        self.eii_helper_path = os.path.join(eii_dep_tool_root, "eii_helper")
        self.eii_helper_temp_path = os.path.join(self.eii_helper_path, "templates")
        self.msg_transfer_station_file = os.path.join(eii_dep_tool_root, "src/utils", 
            "msg_transfer_station.py")

        eii_version = self._get_eii_version()
        self.EII_VERSION = eii_version if eii_version else "2.6"
        
        # For storing password of the host machine
        self._host_pwd = pwd
        # For storing modules configurations
        self._config = None

        self.init_ui()
        self.signals_collector()

        self._update_frame_thread = None

    def init_ui(self):
        self.grid_lyt_global = QGridLayout(self)
        self.grid_lyt_global.setContentsMargins(0, 0, 0, 0)
        self.grid_lyt_global.setVerticalSpacing(0)
        self.grid_lyt_global.setHorizontalSpacing(0)

        self.init_left()
        self.init_middle()
        self.init_right()

    def init_left(self):
        self.wgt_left = QWidget(self)
        self.grid_lyt_left = QGridLayout(self.wgt_left)
        self.grid_lyt_global.addWidget(self.wgt_left, 0, 0, 1, 1)

        self.btn_build = QPushButton(self.wgt_left)
        self.btn_build.setText("&Build")
        self.btn_build.setMinimumSize(QSize(80, 35))
        self.btn_build.setMaximumSize(QSize(80, 35))
        self.btn_build.setStyleSheet(SS_MINI_BTN_ENABLE)
        self.grid_lyt_left.addWidget(self.btn_build, 1, 0, 1, 1)

        self.lbl_arrow_1 = V1ArrowLabel(self.wgt_left, arrow_pos="down", size=8)
        self.lbl_arrow_1.setMinimumSize(QSize(80, 0))
        self.lbl_arrow_1.setMaximumSize(QSize(80, 16777215))
        self.grid_lyt_left.addWidget(self.lbl_arrow_1, 2, 0, 1, 1)

        self.btn_start = QPushButton(self.wgt_left)
        self.btn_start.setText("&Start")
        self.btn_start.setMinimumSize(QSize(80, 35))
        self.btn_start.setMaximumSize(QSize(80, 35))
        self.btn_start.setStyleSheet(SS_MINI_BTN_ENABLE)
        self.btn_start.setEnabled(True)
        self.grid_lyt_left.addWidget(self.btn_start, 3, 0, 1, 1)

        self.lbl_arrow_2 = V1ArrowLabel(self.wgt_left, arrow_pos="down", size=8)
        self.lbl_arrow_2.setMinimumSize(QSize(80, 0))
        self.lbl_arrow_2.setMaximumSize(QSize(80, 16777215))
        self.grid_lyt_left.addWidget(self.lbl_arrow_2, 4, 0, 1, 1)

        self.btn_stop = QPushButton(self.wgt_left)
        self.btn_stop.setText("S&top")
        self.btn_stop.setMinimumSize(QSize(80, 35))
        self.btn_stop.setMaximumSize(QSize(80, 35))
        self.btn_stop.setStyleSheet(SS_MINI_BTN_DISABLE)
        self.grid_lyt_left.addWidget(self.btn_stop, 5, 0, 1, 1)
        self.btn_stop.setEnabled(False)

        self.line_1 = QFrame(self.wgt_left)
        self.line_1.setFrameShape(QFrame.HLine)
        self.line_1.setFrameShadow(QFrame.Sunken)
        self.grid_lyt_left.addWidget(self.line_1, 6, 0, 1, 1)

        self.btn_save = QPushButton(self.wgt_left)
        self.btn_save.setText("S&ave")
        self.btn_save.setMinimumSize(QSize(80, 35))
        self.btn_save.setMaximumSize(QSize(80, 35))
        self.btn_save.setStyleSheet(SS_MINI_BTN_ENABLE)
        self.grid_lyt_left.addWidget(self.btn_save, 7, 0, 1, 1)

        spacer1 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_lyt_left.addItem(spacer1, 0, 0, 1, 1)
        spacer2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.grid_lyt_left.addItem(spacer2, 8, 0, 1, 1)

    def init_middle(self):
        self.wgt_middle = QWidget(self)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wgt_middle.sizePolicy().hasHeightForWidth())
        self.wgt_middle.setSizePolicy(sizePolicy)
        self.grid_lyt_middle = QGridLayout(self.wgt_middle)
        self.grid_lyt_global.addWidget(self.wgt_middle, 0, 1, 1, 1)

        self.lbl_data_stream_name = QLabel(self.wgt_middle)
        self.lbl_data_stream_name.setText(self.project_name)
        font = QFont()
        font.setUnderline(True)
        self.lbl_data_stream_name.setFont(font)
        self.lbl_data_stream_name.setStyleSheet("color: rgb(32, 74, 135);")
        self.grid_lyt_middle.addWidget(self.lbl_data_stream_name, 0, 0, 1, 1)

        self.lbl_component = KeyLabel(self.wgt_middle, win_width=80)
        self.lbl_component.setText("Component")
        self.grid_lyt_middle.addWidget(self.lbl_component, 1, 0, 1, 1)
        
        self.cbbox_component = QComboBox(self.wgt_middle)
        self.grid_lyt_middle.addWidget(self.cbbox_component, 1, 1, 1, 1)

        self.te_data_stream_config = QTextEdit(self.wgt_middle)
        # self.te_data_stream_config.setReadOnly(True)
        self.grid_lyt_middle.addWidget(self.te_data_stream_config, 2, 0, 1, 2)

    def init_right(self):
        self.wgt_right = QWidget(self)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(130)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wgt_right.sizePolicy().hasHeightForWidth())
        self.wgt_right.setSizePolicy(sizePolicy)
        self.grid_lyt_right = QGridLayout(self.wgt_right)
        self.grid_lyt_global.addWidget(self.wgt_right, 0, 2, 1, 1)

        self.lbl_visualizer = QLabel(self.wgt_right)
        self.lbl_visualizer.setText("Visualizer")
        font = QFont()
        font.setUnderline(True)
        self.lbl_visualizer.setFont(font)
        self.lbl_visualizer.setStyleSheet("color: rgb(32, 74, 135);")
        self.grid_lyt_right.addWidget(self.lbl_visualizer, 0, 0, 1, 1)

        self.frm_visualizer = QLabel(self.wgt_right)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.frm_visualizer.sizePolicy().hasHeightForWidth())
        self.frm_visualizer.setStyleSheet("background-color: rgb(186, 189, 182);")
        self.frm_visualizer.setSizePolicy(sizePolicy)
        self.grid_lyt_right.addWidget(self.frm_visualizer, 1, 0, 1, 1)

        self.lbl_result = QLabel(self.wgt_right)
        self.lbl_result.setText("Result")
        font = QFont()
        font.setUnderline(True)
        self.lbl_result.setFont(font)
        self.lbl_result.setStyleSheet("color: rgb(32, 74, 135);")
        self.grid_lyt_right.addWidget(self.lbl_result, 2, 0, 1, 1)

        self.te_result = QTextEdit(self.wgt_right)
        self.te_result.setReadOnly(True)
        self.te_result.setMinimumSize(QSize(00, 90))
        self.te_result.setMaximumSize(QSize(16777215, 90))
        self.grid_lyt_right.addWidget(self.te_result, 3, 0, 1, 1)

    def get_config(self):
        return self._config
    
    def set_config(self, config):
        self._config = config
        all_comps = list(self._config.keys())
        self.cbbox_component.clear()
        self.cbbox_component.addItems(all_comps)
        
        current_comp_config = json.dumps(self._config[all_comps[0]], indent=4)
        self.te_data_stream_config.setText(current_comp_config)
    
    @property
    def _cmd_remove_built_images(self):
        docker_comp_build_file = os.path.join(self.eii_build_path, EII_FILE_NAME.DOCKER_COMPOSE_BUILD.value)
        if not os.path.exists(docker_comp_build_file):
            return "echo none"
        with open(docker_comp_build_file, "r") as f:
            content = yaml.safe_load(f)
            services = set(content["services"])
            no_need = {
                "ia_eiibase", "ia_common", "ia_etcd_ui", 
                "ia_web_visualizer", "ia_imagestore",
            }
            services -= no_need
        new_list = []
        for service in services:
            service = "eiigui/" + service + ":" + self.EII_VERSION
            new_list.append(service)
        to_remove_str = " ".join(new_list)
        print("to remove images: " + to_remove_str)
        
        if new_list:
            cmd = "docker rmi -f {} 2>&1".format(to_remove_str)
        else:
            cmd = "echo none"
        return cmd

    @property
    def _cmd_force_stop_running_containers(self):
        docker_comp_file = os.path.join(self.eii_build_path, EII_FILE_NAME.DOCKER_COMPOSE.value)
        with open(docker_comp_file, "r") as f:
            content = yaml.safe_load(f)
            services = content["services"]
        services_str = " ".join(services)
        print("to stop containers: " + services_str)
        
        cmd = "docker rm -f {} 2>&1".format(services_str)
        return cmd

    @property
    def _eii_cmd_call_builder(self):
        project_component_file = os.path.join(self.project_path, "project_components.yml")
        
        cmd = "cd {} && echo {} | sudo -S -E python3 builder.py -f {} 2>&1".format(
            self.eii_build_path, self._host_pwd, project_component_file)
        return cmd

    @property
    def _eii_cmd_do_provision(self):
        provision_path = os.path.join(self.eii_build_path, "provision")

        cmd = "cd {} && echo {} | sudo -S -E ./provision.sh ../docker-compose.yml 2>&1".format(
            provision_path, self._host_pwd)
        return cmd

    @property
    def _eii_cmd_build_images(self):
        docker_comp_build_file = os.path.join(self.eii_build_path, EII_FILE_NAME.DOCKER_COMPOSE_BUILD.value)
        if not os.path.exists(docker_comp_build_file):
            return "echo none"
        with open(docker_comp_build_file, "r") as f:
            content = yaml.safe_load(f)
            services = set(content["services"])
            no_need = {
                "ia_eiibase", "ia_common", "ia_video_common", 
                "ia_openvino_base", "ia_etcd_ui", 
                "ia_video_ingestion", "ia_video_analytics", 
                "ia_visualizer", "ia_web_visualizer", "ia_imagestore",
            }
            services -= no_need
        to_build_images = " ".join(services)
        cmd = "cd {} && docker-compose -f docker-compose-build.yml build ".format(
            self.eii_build_path)
        build_cmd = cmd + to_build_images
        # print("------------")
        # print(build_cmd)
        # print("------------")
        return build_cmd

    @property
    def _eii_cmd_start_containers(self):
        cmd = "cd {} && docker-compose up -d 2>&1".format(self.eii_build_path)
        return cmd

    @property
    def _eii_cmd_stop_containers(self):
        cmd = "cd {} && docker-compose down 2>&1".format(self.eii_build_path)
        return cmd

    @property
    def _eii_cmd_run_msg_transfer_station(self):
        args = ""
        subscriber = self._config["WebVisualizer"]["interfaces"]["Subscribers"][0]
        global datastream_labels
        datastream_labels = self._config["WebVisualizer"]["config"]["labels"].values()
        topic = subscriber["Topics"][0]
        if subscriber["Type"] == "zmq_ipc":
            args = " {} {} ".format("ipc", topic)
        else:
            endpoint = subscriber["EndPoint"]
            host, port = endpoint.split(":")
            args = " {} {} {} {} ".format("tcp", topic, host, port)

        cmd = "echo {} | sudo -S PYTHONPATH=$PYTHONPATH:/usr/lib/python3.8/site-packages LD_LIBRARY_PATH=/opt/intel/eii/lib python3 {} {}".format(self._host_pwd, 
            self.msg_transfer_station_file, args)
        return cmd
    
    @staticmethod
    def _join_cmds(cmds_list):
        return " && ".join(cmds_list)

    @property
    def _echo_green_text_fmt(self):
        return " echo \"\033[32m{}\033[0m\" "

    @property
    def _echo_blue_text_fmt(self):
        return " echo \"\033[34m{}\033[0m\" "
    
    def _stop_msg_transfer_station_thread(self):
        # It's really ugly!
        cmd = "echo {} | sudo -S pkill -9 -f {} > /dev/null".format(
            self._host_pwd, self.msg_transfer_station_file)
        sp.call(cmd, shell=True)
    
    def slot_current_component_changed(self, comp_name):
        if comp_name != "":
            comp_config = json.dumps(self._config[comp_name], indent=8)
            self.te_data_stream_config.setText(comp_config)

    def slot_btn_build(self):
        self.log_console.show()
        self.log_console.clear_text()
        self.root_win.activateWindow()

        empty_line_cmd = self._echo_green_text_fmt.format(" ")
        post_text_cmd = self._echo_green_text_fmt.format("Build images successfully!")
        wait_cmd = "wait && sleep 1"

        # Do the following work in the EII build path
        # And commands' log should be tracked
        os.system(self._eii_cmd_call_builder)
        total_cmds = self._join_cmds([
            self._cmd_remove_built_images,
            self._eii_cmd_do_provision,
            self._eii_cmd_build_images,
            empty_line_cmd, post_text_cmd, wait_cmd
        ])
        proc = sp.Popen(total_cmds, shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
        self.log_proc_flag.t = proc

    def slot_btn_start(self):
        # Update btn status
        self.btn_start.setEnabled(False)
        self.btn_start.setStyleSheet(SS_MINI_BTN_DISABLE)
        self.btn_stop.setEnabled(True)
        self.btn_stop.setStyleSheet(SS_MINI_BTN_ENABLE)

        self.log_console.show()
        self.log_console.clear_text()
        self.root_win.activateWindow()
        self._stop_msg_transfer_station_thread()

        self._update_frame_thread = UpdateFrameThread()
        self._update_frame_thread.change_frame.connect(self.slot_update_frame)
        self._update_frame_thread.start()

        empty_line_cmd = self._echo_green_text_fmt.format(" ")
        post_text_cmd = self._echo_green_text_fmt.format("Start services successfully!")
        wait_cmd = "wait && sleep 0"

        # Remove config tmp file
        rm_tmp_file_cmd = "echo {} | sudo -S rm -f /var/tmp/config.json".format(self._host_pwd)

        # Do the following work in the EII build path
        # And commands' log should be tracked
        total_cmds = self._join_cmds([
            rm_tmp_file_cmd,
            self._eii_cmd_call_builder,
            self._cmd_force_stop_running_containers,
            self._eii_cmd_do_provision,
            self._eii_cmd_start_containers,
            empty_line_cmd, post_text_cmd, wait_cmd,
            self._eii_cmd_run_msg_transfer_station,
        ])
        proc = sp.Popen(total_cmds, shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
        self.log_proc_flag.t = proc

    def slot_btn_stop(self):
        # Update btn status
        self.btn_start.setEnabled(True)
        self.btn_start.setStyleSheet(SS_MINI_BTN_ENABLE)
        self.btn_stop.setEnabled(False)
        self.btn_stop.setStyleSheet(SS_MINI_BTN_DISABLE)

        self.log_console.show()
        self.log_console.clear_text()
        self.root_win.activateWindow()
        if self._update_frame_thread:
            self._update_frame_thread.stop()
        self._stop_msg_transfer_station_thread()

        empty_line_cmd = self._echo_green_text_fmt.format(" ")
        post_text_cmd = self._echo_green_text_fmt.format("Stop services successfully!")
        wait_cmd = "wait && sleep 1"

        # Do the following work in the EII build path
        # And commands' log should be tracked
        total_cmds = self._join_cmds([
            self._eii_cmd_call_builder,
            self._cmd_force_stop_running_containers,
            empty_line_cmd, post_text_cmd, wait_cmd
        ])
        proc = sp.Popen(total_cmds, shell=True, stdout=sp.PIPE, stderr=sp.STDOUT)
        self.log_proc_flag.t = proc

    def slot_btn_save_clicked(self):
        current_config_changed = False
        current_comp_name = self.cbbox_component.currentText()

        current_config = self.te_data_stream_config.toPlainText()
        if self._is_json_valid(current_config):
            init_config_dict = self._config[current_comp_name]
            if json.loads(current_config) != init_config_dict:
                current_config_changed = True
                # Write changes to config.json
                config_file = os.path.join(self.project_path, current_comp_name, "config.json")
                with open(config_file, "w") as f:
                    f.write(json.dumps(json.loads(current_config), indent=4))
                # Save changes to _config
                self._config[current_comp_name] = json.loads(current_config)
                # Show dialog
                QMessageBox.information(self, "Info", "Change saved successfully.", QMessageBox.Ok)
        else:
            QMessageBox.warning(self, "Warning", 
                "The json format of \"{}\" is invalid.".format(current_comp_name), QMessageBox.Ok)
            return

        if not current_config_changed:
            QMessageBox.information(self, "Info", "No change need to save.", QMessageBox.Ok)
    
    def slot_btn_prev(self):
        self.log_console.hide()
        if self._update_frame_thread:
            self._update_frame_thread.stop()
        self._stop_msg_transfer_station_thread()
    
    def slot_btn_next(self):
        self.log_console.hide()
        if self._update_frame_thread:
            self._update_frame_thread.stop()
        self._stop_msg_transfer_station_thread()
    
    def slot_btn_home(self):
        self.log_console.hide()
        if self._update_frame_thread:
            self._update_frame_thread.stop()
        self._stop_msg_transfer_station_thread()
    
    def slot_update_frame(self, data):
        pix, metadata = data
        pix_scaled = pix.scaled(
            self.frm_visualizer.width(),
            self.frm_visualizer.height(),
            Qt.KeepAspectRatio
        )
        self.frm_visualizer.setPixmap(pix_scaled)
        self.te_result.setText(json.dumps(metadata))
    
    def signals_collector(self):
        self.cbbox_component.currentTextChanged.connect(self.slot_current_component_changed)
        self.btn_build.clicked.connect(self.slot_btn_build)
        self.btn_start.clicked.connect(self.slot_btn_start)
        self.btn_stop.clicked.connect(self.slot_btn_stop)
        self.btn_save.clicked.connect(self.slot_btn_save_clicked)
    
    def closeEvent(self, e):
        if self._update_frame_thread:
            self._update_frame_thread.stop()

    @staticmethod
    def _is_json_valid(json_content):
        try:
            json_object = json.loads(json_content)
        except ValueError as e:
            return False
        return True

    def _get_eii_version(self):
        try:
            env_file = os.path.join(self.eii_build_path, ".env")
            with open(env_file, "r") as f:
                lines = f.readlines()
                for line in lines:
                    if "EII_VERSION" in line:
                        version = line.strip().split("=")[-1]
                        return version
        except:
            return ""


class UpdateFrameThread(QThread):

    change_frame = Signal(object)

    def __init__(self, parent=None):
        super(UpdateFrameThread, self).__init__(parent=parent)
        self._run_flag = True

    def run(self):
        msgbus_cfg = {"type": "zmq_tcp", "transfer_station": {"host": "0.0.0.0", "port": 55005}}
        msgbus = mb.MsgbusContext(msgbus_cfg)
        subscriber = msgbus.new_subscriber("transfer_station")

        while self._run_flag:
            metadata, blob = subscriber.recv()
            frame = self.decode_frame(metadata, blob)
            # Draw defects
            if datastream_labels:
                labels = [v for v in datastream_labels][0]
                self.draw_defect(metadata, frame, labels)
            else:
                self.draw_defect(metadata, frame, None)
            h, w, c = frame.shape
            qimg = QImage(frame.data, w, h, 3 * w, QImage.Format_RGB888).rgbSwapped()
            pix = QPixmap(qimg)
            self.change_frame.emit((pix, metadata))
        subscriber.close()

    def stop(self):
        self._run_flag = False
        self.wait()

    @staticmethod
    def decode_frame(results, blob):
        height = int(results["height"])
        width = int(results["width"])
        channels = int(results["channels"])
        encoding = None

        if "encoding_type" and "encoding_level" in results:
            encoding = {"type": results["encoding_type"],
                        "level": results["encoding_level"]}
        # Convert to Numpy array and reshape to frame
        if isinstance(blob, list):
            # If multiple frames, select first frame for 
            # visualization
            blob = blob[0]
        frame = np.frombuffer(blob, dtype=np.uint8)
        if encoding is not None:
            frame = np.reshape(frame, (frame.shape))
            try:
                frame = cv2.imdecode(frame, 1)
            except cv2.error as ex:
                pass
        else:
            frame = np.reshape(frame, (height, width, channels))

        return frame

    def draw_defect(self, results, frame, stream_label=None):
        """Draw boxes on the frames

        :param results: Metadata of frame received from message bus.
        :type: dict
        :param frame: Classified frame.
        :type: numpy
        :param stream_label: Message received on the given topic (JSON blob)
        :type: str
        """
        # Define color
        self.good_color = (0, 255, 0)
        self.bad_color = (0, 0, 255)

        # Display information about frame FPS
        x_cord = 40
        y_cord = 80
        for res in results:
            if "Fps" in res or "time" in res:
                if "time" in res:
                    # display inference_fps 
                    fps_str = "{} : {}frame/sec".format(str(res).split('_')[0] + '_fps', str(round(1/results[res], 5)))
                else:
                    fps_str = "{} : {}frame/sec".format(str(res), str(results[res]))
                
                cv2.putText(frame, fps_str, (x_cord, y_cord),
                            cv2.FONT_HERSHEY_DUPLEX, 2,
                            (100, 100, 100), 3, cv2.LINE_AA)
                            
                cv2.putText(frame, fps_str, (x_cord, y_cord),
                            cv2.FONT_HERSHEY_DUPLEX, 2,
                            self.good_color, 2, cv2.LINE_AA)
                y_cord = y_cord + 60

        # Draw defects
        if 'defects' in results:
            for defect in results['defects']:
                defect['tl'][0] = int(defect['tl'][0])
                defect['tl'][1] = int(defect['tl'][1])
                defect['br'][0] = int(defect['br'][0])
                defect['br'][1] = int(defect['br'][1])

                # Get tuples for top-left and bottom-right coordinates
                top_left = tuple(defect['tl'])
                bottom_right = tuple(defect['br'])

                # Draw bounding box
                cv2.rectangle(frame, top_left,
                              bottom_right, self.bad_color, 2)

                # Draw labels for defects if given the mapping
                if stream_label is not None:
                    # Position of the text below the bounding box
                    pos = (top_left[0], bottom_right[1] + 20)

                    # The label is the "type" key of the defect, which
                    #  is converted to a string for getting from the labels
                    if str(defect['type']) in stream_label:
                        label = stream_label[str(defect['type'])]
                        cv2.putText(frame, label, pos,
                                    cv2.FONT_HERSHEY_DUPLEX,
                                    1.5, self.bad_color, 2, cv2.LINE_AA)
                    else:
                        cv2.putText(frame, str(defect['type']), pos,
                                    cv2.FONT_HERSHEY_DUPLEX,
                                    1.5, self.bad_color, 2, cv2.LINE_AA)

            # Draw border around frame if has defects or no defects
            if results['defects']:
                outline_color = self.bad_color
            else:
                outline_color = self.good_color

            frame = cv2.copyMakeBorder(frame, 5, 5, 5, 5, cv2.BORDER_CONSTANT,
                                       value=outline_color)

        # Display information about frame
        (d_x, d_y) = (20, 50)
        if 'display_info' in results:
            for d_i in results['display_info']:
                # Get priority
                priority = d_i['priority']
                info = d_i['info']
                d_y = d_y + 10

                #  LOW
                if priority == 0:
                    cv2.putText(frame, info, (d_x, d_y),
                                cv2.FONT_HERSHEY_DUPLEX,
                                0.5, (0, 255, 0), 1, cv2.LINE_AA)
                #  MEDIUM
                if priority == 1:
                    cv2.putText(frame, info, (d_x, d_y),
                                cv2.FONT_HERSHEY_DUPLEX,
                                0.5, (0, 150, 170), 1, cv2.LINE_AA)
                #  HIGH
                if priority == 2:
                    cv2.putText(frame, info, (d_x, d_y),
                                cv2.FONT_HERSHEY_DUPLEX,
                                0.5, (0, 0, 255), 1, cv2.LINE_AA)

