import sys
from PySide2.QtWidgets import *
from PySide2 import QtCore, QtGui, QtWidgets
from welcome_win import WelcomeWin


class DeploymentTool(QWidget):

    def __init__(self, parent=None):
        super(DeploymentTool, self).__init__(parent=parent)
        self.setWindowTitle("EII Deployment Tool")
        self.resize(1200, 720)
        self.grid_lyt_global = QGridLayout(self)

        self.win_welcome = WelcomeWin(root_win=self)
        self.grid_lyt_global.addWidget(self.win_welcome, 0, 0, 1, 1)

        # Move window to the central of screen (old way)
        # screen = QDesktopWidget().screenGeometry()
        # screen_w = screen.width()
        # screen_h = screen.height()
        # window_w = self.frameSize().width()
        # window_h = self.frameSize().height()
        # self.move(int(screen_w * 0.5) - int(window_w * 0.5),
        #           int(screen_h * (1 - 0.58)) - int(window_h * 0.5))

    def closeEvent(self, e):
        win_main_item = self.grid_lyt_global.itemAtPosition(1, 0)
        if win_main_item:
            win_main = win_main_item.widget()
            if win_main._log_update_thread and win_main._log_update_thread.isRunning():
                if win_main._log_update_thread.proc and win_main._log_update_thread.proc.poll() is None:
                    win_main._log_update_thread.proc.kill()
                win_main._log_update_thread.terminate()

            win_main._log_console.close()

def centerWindow(widget):
    window = widget.window()
    window.setGeometry(
        QtWidgets.QStyle.alignedRect(
            QtCore.Qt.LeftToRight,
            QtCore.Qt.AlignCenter,
            window.size(),
            QtGui.QGuiApplication.primaryScreen().availableGeometry(),
        ),
    )

if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = DeploymentTool()
    centerWindow(win)
    win.show()
    sys.exit(app.exec_())
