# Copyright (c) 2020 Intel Corporation.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

version: '3.6'

services:
  ## Define TcpExport service
  {{F_PROJECT_NAME}}_tcp_export:
    build:
      context: $PWD/../eii_helper/workspace/{{F_PROJECT_NAME}}/TcpExport
      dockerfile: $PWD/../eii_helper/workspace/{{F_PROJECT_NAME}}/TcpExport/Dockerfile
      args:
        EII_VERSION: ${EII_VERSION}
        EII_UID: ${EII_UID}
        EII_USER_NAME: ${EII_USER_NAME}
        DOCKER_REGISTRY: ${DOCKER_REGISTRY}
        UBUNTU_IMAGE_VERSION: ${UBUNTU_IMAGE_VERSION}
        CMAKE_INSTALL_PREFIX: ${EII_INSTALL_PATH}
        # set CMAKE_BUILD_TYPE value to "Debug" to generate debug symbols
        CMAKE_BUILD_TYPE: "Release"
    read_only: true
    user: root
    network_mode: host
    image: ${DOCKER_REGISTRY}eiigui/{{F_PROJECT_NAME}}_tcp_export:${EII_VERSION}
    container_name: {{F_PROJECT_NAME}}_tcp_export
    hostname: {{F_PROJECT_NAME}}_tcp_export
    restart: unless-stopped
    security_opt:
    - no-new-privileges
    healthcheck:
      test: ["CMD-SHELL", "exit", "0"]
      interval: 5m
    environment:
      AppName: "{{F_PROJECT_NAME}}_TcpExport"
      DEV_MODE: ${DEV_MODE}
      no_proxy: {{F_ETCD_HOST}}
      ETCD_HOST: {{F_ETCD_HOST}}
      ETCD_CLIENT_PORT: ${ETCD_CLIENT_PORT}
      ETCD_PREFIX: ${ETCD_PREFIX}
      # MessageBus Endpoint Configuration
      CertType: "zmq"
      # ZMQ_RECV_HWM: "1000"

    volumes:
      - "vol_eii_socket:${SOCKET_DIR}"
    secrets:
      - ca_etcd
      - etcd_{{F_PROJECT_NAME}}_TcpExport_cert
      - etcd_{{F_PROJECT_NAME}}_TcpExport_key

secrets:
  etcd_PythonSubscriber_cert:
    file: provision/Certificates/{{F_PROJECT_NAME}}_TcpExport/{{F_PROJECT_NAME}}_TcpExport_client_certificate.pem
  etcd_PythonSubscriber_key:
    file: provision/Certificates/{{F_PROJECT_NAME}}_TcpExport/{{F_PROJECT_NAME}}_TcpExport_client_key.pem
