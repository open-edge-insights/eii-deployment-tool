#!/bin/bash

set -e
sudo -v

CUR_DIR=$PWD
EIIMessageBus=$PWD/EIIMessageBus
EIIUtils=$PWD/EIIUtils
IntelSafeString=$PWD/EIIUtils/IntelSafeString

export CMAKE_INSTALL_PREFIX="/opt/intel/eii"
# Set dynamic lib path
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CMAKE_INSTALL_PREFIX/lib
# Set C/C++ include path
export C_INCLUDE_PATH=$CMAKE_INSTALL_PREFIX/include
export CPLUS_INCLUDE_PATH=$CMAKE_INSTALL_PREFIX/include

# Install cmake
sudo apt update
sudo apt install -y cmake

# Build & Install IntelSafeString
cd $IntelSafeString
rm -rf build
mkdir build
cd build
cmake -DCMAKE_INSTALL_INCLUDEDIR=$CMAKE_INSTALL_PREFIX/include -DCMAKE_INSTALL_PREFIX=$CMAKE_INSTALL_PREFIX ..
make
sudo -E make install

# Install EIIUtils deps
cd $EIIUtils
sudo -E ./install.sh

# Build & Install EIIUtils
cd $EIIUtils
rm -rf build
mkdir build
cd build
cmake -DCMAKE_INSTALL_INCLUDEDIR=$CMAKE_INSTALL_PREFIX/include -DCMAKE_INSTALL_PREFIX=$CMAKE_INSTALL_PREFIX -DWITH_TESTS=ON ..
make
sudo -E make install

# Install EIIMessageBus deps
cd $EIIMessageBus
sudo -H -E ./install.sh --cython

sudo apt install -y python3-sphinx
sudo -H -E pip3 install m2r

# Build & Install EIIMessageBus
cd $EIIMessageBus
rm -rf build
mkdir build
cd build
cmake -DCMAKE_INSTALL_INCLUDEDIR=$CMAKE_INSTALL_PREFIX/include -DCMAKE_INSTALL_PREFIX=$CMAKE_INSTALL_PREFIX -DWITH_PYTHON=ON -DWITH_TESTS=ON ..
sudo -E make
sudo -E make install

# Install EIIMessageBus python pkg
cd $EIIMessageBus/python
sudo -H -E python3 setup.py install --prefix=/usr
