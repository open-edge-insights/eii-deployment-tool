#!/bin/bash

# Current working dir
working_dir=$(pwd)
group=docker

# Variable for displaying log
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
MAGENTA=$(tput setaf 5)
NC=$(tput sgr0)
BOLD=$(tput bold)
INFO=$(tput setaf 3)

set -e

Usage()
{
	echo
	echo "${BOLD}${INFO}==================================================================================${NC}"
	echo
	echo "${BOLD}${GREEN}Usage :: ./usb_cash.sh [OPTION...] ${NC}"
	echo
	echo "${INFO}List of available options..."
    echo
	echo "${INFO}--help		display this help and exit"${NC}
    echo
        echo "${INFO}-h		display this help and exit"${NC}
    echo
	echo "${INFO}--load		load the deploy images by eiigui and than start eii servers"${NC}
        echo "${BOLD}${GREEN}		Note : loading images is the first thing to do, and it only needs to be done once!${NC}"
    echo
	echo "${INFO}--start		start eii containers"${NC}
    echo
	echo "${INFO}--stop		stop eii containers"${NC}
    echo
	echo "${INFO}--remove	delete all the deploy images by eiigui"${NC}
    echo
	echo "${INFO}===================================================================================${NC}"
	exit 1
}

parse_commandLine_args()
{
	if [ $# == "0" ];then
		return;
	fi

	# echo "${INFO}Reading the command line args...${NC}"
	for ARGUMENT in "$@"
	do
	    KEY=$(echo $ARGUMENT | cut -f1 -d=)
	    VALUE=$(echo $ARGUMENT | cut -f2 -d=)

	    case "$KEY" in
		    --help) Usage ;;
		        -h) Usage ;;
		    --load) exec sg $group "./run_case.sh --load" ;;
		    --start) exec sg $group "./run_case.sh --start" ;;
		    --stop) exec sg $group "./run_case.sh --stop" ;;
		    --remove) exec sg $group "./run_case.sh --remove" ;;
		     *) echo "${RED}Invalid arguments passed..${NC}"; Usage; ;;
	    esac
	done
}


if [ $# -ne "0" ];then
	parse_commandLine_args "$@"
else
	echo "${RED}At least one parameter is required${NC}"
	exit 1
fi

