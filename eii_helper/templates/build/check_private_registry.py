import os
import sys
import json
import subprocess as sp


def print_usage():
    if len(sys.argv) < 3:
        file_name = os.path.basename(__file__)
        print("Usage:")
        print("  sudo python3 {} <registry_ip> <registry_port>".format(file_name))
        sys.exit(1)
    

def main():
    print_usage()

    # Flag for judging whether to restart docker
    no_changed = True

    daemon_file = "/etc/docker/daemon.json"
    registry_url = "{}:{}".format(sys.argv[1], sys.argv[2])
    if not os.path.exists(daemon_file):
        with open(daemon_file, "w") as f:
            content = {
                "insecure-registries": [registry_url]
            }
            f.write(json.dumps(content, indent=4))
        no_changed = False
    else:
        with open(daemon_file, "r") as f:
            content = json.loads(f.read())
        if "insecure-registries" in content.keys():
            if registry_url not in content["insecure-registries"]:
                content["insecure-registries"].append(registry_url)
                with open(daemon_file, "w") as f:
                    f.write(json.dumps(content, indent=4))
                no_changed = False
        else:
            content["insecure-registries"] = registry_url
            with open(daemon_file, "w") as f:
                f.write(json.dumps(content, indent=4))
            no_changed = False

    # Add the hostname of registry server to the no_proxy,
    # even if the the client not behind the proxy
    service_dir = "/etc/systemd/system/docker.service.d"
    os.makedirs(service_dir, exist_ok=True)
    for file_name in ["http-proxy.conf", "https-proxy.conf"]:
        http_proxy_conf = os.path.join(service_dir, file_name)
        if not os.path.exists(http_proxy_conf):
            with open(http_proxy_conf, "w") as f:
                content = "[Service]\nEnvironment=\"NO_PROXY=localhost,127.0.0.1,{}\"\n".format(sys.argv[1])
                f.write(content)
            no_changed = False
        else:
            with open(http_proxy_conf, "r") as f:
                content = f.read()
            if "NO_PROXY=" in content:
                if sys.argv[1] not in content:
                    content = content.replace("NO_PROXY=", "NO_PROXY={},".format(sys.argv[1]))
                    with open(http_proxy_conf, "w") as f:
                        f.write(content)
                    no_changed = False
            else:
                if "[Service]" in content:
                    content += "\nEnvironment=\"NO_PROXY=localhost,127.0.0.1,{}\"\n".format(sys.argv[1])
                else:
                    content = "[Service]\nEnvironment=\"NO_PROXY=localhost,127.0.0.1,{}\"\n".format(sys.argv[1])
                with open(http_proxy_conf, "w") as f:
                    f.write(content)
                no_changed = False

    # Flush changes and restart docker
    if not no_changed:
        cmd = ["sudo", "systemctl", "daemon-reload"]
        sp.call(cmd, shell=False)

        cmd = ["sudo", "systemctl", "restart", "docker"]
        sp.call(cmd, shell=False)


if __name__ == "__main__":
    main()
