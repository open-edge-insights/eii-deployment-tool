SS_LBL_ACTIVATED = \
"""
background-color: rgb(237, 212, 0);
"""

SS_LBL_DEACTIVATED = \
"""
background-color: rgb(186, 189, 182);
"""

SS_MINI_BTN_ENABLE = \
"""
QPushButton {
    background-color: rgb(0, 125, 196);
    color: rgb(255, 255, 255);
    border: 2px groove gray;
    border-radius: 2px;
    padding:2px 4px;
}
QPushButton:hover {
    background-color: rgb(170, 110, 0);
}
"""

SS_MINI_BTN_DISABLE = \
"""
QPushButton {
    background-color: rgb(186, 189, 182);
    color: rgb(0, 0, 0);
    border: 2px groove gray;
    border-radius: 2px;
    padding:2px 4px;
}
"""

SS_STD_BTN_ENABLE = \
"""
background-color: rgb(0, 125, 196);
color: rgb(255, 255, 255);
border: 2px groove gray;
border-radius: 6px;
padding:2px 4px;
"""

SS_STD_BTN_DISABLE = \
"""
background-color: rgb(186, 189, 182);
color: rgb(0, 0, 0);
border: 2px groove gray;
border-radius: 6px;
padding:2px 4px;
"""

SS_BIG_BTN_ENABLE = \
"""
background-color: rgb(0, 125, 196);
color: rgb(255, 255, 255);
border: 2px groove gray;
border-radius: 10px;
padding:2px 4px;
"""

SS_BIG_BTN_DISABLE = \
"""
background-color: rgb(186, 189, 182);
color: rgb(0, 0, 0);
border: 2px groove gray;
border-radius: 10px;
padding:2px 4px;
"""

SS_LE_READ_ONLY = \
"""
color: #808080;
background-color: #F0F0F0;
"""

SS_CKBOX_READ_ONLY = \
"""
color: #808080;
background-color: #F0F0F0;
"""

SS_CKBOX_BLACK_INDICATOR = \
"""
QCheckBox::indicator
{
    width: 18px;
    height: 18px;
}
QCheckBox::indicator::unchecked
{
    border: 1px solid black;
}
"""

SS_CKBOX_WHITE_INDICATOR = \
"""
QCheckBox::indicator
{
    width: 18px;
    height: 18px;
}
QCheckBox::indicator::unchecked
{
    border: 1px solid white;
}
"""

SS_TAB_WGT = \
"""
QTabBar::tab { 
    background-color: rgb(186, 189, 182);
    color: rgb(0, 0, 0);
    border: 2px groove gray;
    border-radius: 6px;
    padding:2px 4px;
    height: 25px;
}
QTabBar::tab:selected { 
    background: rgb(0, 125, 196);
    color: rgb(255, 255, 255);
}
"""
