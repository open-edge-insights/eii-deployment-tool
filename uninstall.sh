#!/bin/bash

set -e
sudo -v

EII_GUI_PATH=$PWD

uninstall_eii_gui () {
     # Remove virtualenv 'eiigui-env'
     if [ -f /usr/local/bin/virtualenvwrapper.sh ]; then
        source /usr/local/bin/virtualenvwrapper.sh
        rmvirtualenv eiigui-env
     fi

     # Remove eiigui env in '~/.bashrc'
     sed -i '/Python virtual envrionment settings/d' ~/.bashrc
     sed -i '/WORKON_HOME/d' ~/.bashrc
     sed -i '/VIRTUALENVWRAPPER_PYTHON/d' ~/.bashrc
     sed -i '/virtualenvwrapper.sh/d' ~/.bashrc

     sed -i '/EII GUI settings/d' ~/.bashrc
     sed -i '/EII_GUI_HOME/d' ~/.bashrc

     sed -i '/Genicam and gstreamer settings/d' ~/.bashrc
     sed -i '/GENICAM_GENTL64_PATH/d' ~/.bashrc
     sed -i '/GST_PLUGIN_PATH/d' ~/.bashrc

     sed -i '/EIIMessageBus dynamic lib path settings/d' ~/.bashrc
     sed -i '/LD_LIBRARY_PATH=:\/opt\/intel\/eii\/lib/d' ~/.bashrc
     sed -i '/PYTHONPATH=$PYTHONPATH:\/usr\/lib\/python3.8\/site-packages/d' ~/.bashrc

     # Remove private registry image
     docker rm -f eiigui-registry
     docker rmi -f registry:2

     # Remove eiigui docker images
     docker rm -f $(docker ps | grep "eiigui" | awk '{print $1}')
     docker rm -f $(docker ps | grep "openedgeinsights" | awk '{print $1}')
     docker rmi -f $(docker images | grep "eiigui" | awk '{print $3}')

     # Remove download things
     EII_MSGBUS = $EII_GUI_PATH/setup/eii-msgbus/EIIMessageBus
     EII_UTILS = $EII_GUI_PATH/setup/eii-msgbus/EIIUtils
     GSTREAM_PLUGIN = $EII_GUI_PATH/setup/src-gst-gencamsrc
     if [ -d "$EII_MSGBUS" ]; then
         rm -rf $EII_MSGBUS
     fi
     if [ -d "$EII_UTILS" ]; then
         rm -rf $EII_UTILS
     fi
     if [ -d "$GSTREAM_PLUGIN" ]; then
         rm -rf $GSTREAM_PLUGIN
     fi
}

uninstall_eii_gui
